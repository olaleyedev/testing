describe("should test about page", () => {
	it("renders about page", () => {
		cy.visit("/about");
		cy.get("h1").contains(/about/i);
	});
});
