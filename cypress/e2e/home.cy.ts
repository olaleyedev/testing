describe("should test home page", () => {
  it("should test home page", () => {
    cy.visit("/");
    cy.get("h1").contains(/testing fireforce/i);    
  });

  it("should go to about page", () => {
    cy.visit("/");
    cy.get("a").contains(/about/i).click();
    cy.url().should("include", "/about");
  });
});
