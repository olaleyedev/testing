import { defineConfig } from "cypress";

export default defineConfig({
  projectId: 'qf8459',
	e2e: {
		baseUrl: "http://localhost:3000",
		setupNodeEvents(on, config) {
			// implement node event listeners here
		},
	},
	video: false,
	videoUploadOnPasses: false,
});
