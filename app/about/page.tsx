import React from 'react'

const Page = () => {
  return (
    <>
      <header><h1>About Page</h1></header>
      <main>
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sint tenetur aspernatur eos alias distinctio libero adipisci aperiam hic fuga, iusto ex, illum, reiciendis tempore. Omnis earum aut, maiores ipsum quos accusamus laboriosam quam nisi corporis ad. Dolore possimus fuga unde?
      </main>
    </>
  )
}

export default Page
